import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'hogwarts',
    pathMatch: 'full'
  },
  {
    path: 'hogwarts',
    loadChildren: ()  => import('./public/hogwarts/hogwarts.module').then(m => m.HogwartsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
