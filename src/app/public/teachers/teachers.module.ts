import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachersComponent } from './teachers.component';
import { TeachersRoutingModule } from './teachers-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TableModule } from '../components/table/table.module';

@NgModule({
  declarations: [TeachersComponent],
  imports: [
    CommonModule,
    TeachersRoutingModule,
    NgxSpinnerModule,
    TableModule
  ]
})
export class TeachersModule { }
