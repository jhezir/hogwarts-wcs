import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IPerson } from '../models/person.model';
import { HogwartsService } from '../services/hogwarts.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {
  public dataSource: MatTableDataSource<IPerson>;
  constructor(
    private hogwartsService: HogwartsService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.getTeachers();

  }
  // Funtion get teachers
  public getTeachers(): void {
    this.hogwartsService.getTeachers().then((response: Array<IPerson>) => {
      this.dataSource = new MatTableDataSource(response);
      console.log('this.dataSource', this.dataSource);
      this.spinner.hide();
    }).catch((error: any) => {
      this.spinner.hide();
      console.log('error', error);
    });
  }
}
