import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IPerson } from '../models/person.model';

@Injectable({
  providedIn: 'root'
})
export class HogwartsService {

  constructor(private http: HttpClient) { }

  /**
   * Service getCharacters 
   * param: house
   */
  public getCharacters(house: string): Promise<any> {
    return this.http.get<Array<IPerson>>(environment.urlApi + '/house/' + house).toPromise();
  }

  /**
   * Service getStudents
   */
  public getStudents(): Promise<any> {
    return this.http.get<Array<IPerson>>(environment.urlApi + '/students').toPromise();
  }

  /**
   * Service getTeachers
   */
  public getTeachers(): Promise<Array<IPerson>> {
    return this.http.get<Array<IPerson>>(environment.urlApi + '/staff').toPromise();
  }

}
