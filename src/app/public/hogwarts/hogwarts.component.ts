import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-hogwarts',
  templateUrl: './hogwarts.component.html',
  styleUrls: ['./hogwarts.component.scss']
})
export class HogwartsComponent implements OnInit {
  links = [];
  activeLink = this.links[0];
  constructor() { }

  ngOnInit(): void {
    this.links = [{ name: 'Characters', url: '/hogwarts/characters' }, { name: 'Students', url: '/hogwarts/students' }, { name: 'Teachers', url: '/hogwarts/teachers' }];

  }
}
