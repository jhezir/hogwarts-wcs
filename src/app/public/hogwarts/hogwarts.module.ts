import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HogwartsComponent } from './hogwarts.component';
import { HogwartsRoutingModule } from './hogwarts-routing.module';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [HogwartsComponent],
  imports: [
    CommonModule,
    HogwartsRoutingModule,
    MatTabsModule
  ]
})
export class HogwartsModule { }
