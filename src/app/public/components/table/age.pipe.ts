import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'age' })
export class Age implements PipeTransform {
    transform(value:string) {
        console.log(value)
        if (value)
            return moment().diff(value, 'years');
        else return '-';
    }
}