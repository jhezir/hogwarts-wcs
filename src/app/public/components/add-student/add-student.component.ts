import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {
  public form: FormGroup;
  @Output() studentResponse = new EventEmitter();

  constructor(
    private fb: FormBuilder,

  ) { }

  ngOnInit(): void {
    this.createForm();
  }
  public createForm() {
    this.form = this.fb.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      patronus: new FormControl('', Validators.compose([
        Validators.required
      ])),
      age: new FormControl(0, Validators.compose([
        Validators.required
      ])),
      image: new FormControl(null, []),

    });
  }
  public save() {
    console.log('da')
    this.studentResponse.emit(this.form.value);
    this.form.reset();
  }

  public close() {
    this.form.reset();

  }

  /**
   * handle file from browsing
   */
  public fileBrowseHandler(files) {
    console.log(files);
    if (files[0].type === "image/png" && files[0].size < 1024000) {
      const img: any = document.querySelector("#fileDropRef");
      if (typeof FileReader !== "undefined") {
        const reader = new FileReader();
        reader.onload = this._handleReaderLoad.bind(this);
        reader.readAsDataURL(files[0]);
      };
    }

  }
  private _handleReaderLoad(e: any) {
    const reader: any = e.target;
    const base: any = reader.result;
    this.form.controls.image.setValue(base);

  }

}
