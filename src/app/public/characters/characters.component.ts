import { Component, OnInit } from '@angular/core';
import { IPerson } from '../models/person.model';
import { HogwartsService } from '../services/hogwarts.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss'],
  providers: [HogwartsService]
})
export class CharactersComponent implements OnInit {

  public house: Array<string> = ['slytherin', 'gryffindor', 'ravenclaw', 'hufflepuff'];
  public dataSource: MatTableDataSource<IPerson>;
  public selected: FormControl;

  constructor(
    private hogwartsService: HogwartsService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.getCharacters(this.house[0]);
    this.selected = new FormControl('slytherin');
    this.selected.valueChanges.subscribe((value: string) => {
      this.getCharacters(value);
    })

  }
  // Funtion get characters
  public getCharacters(house: string): void {
    this.hogwartsService.getCharacters(house).then((response: Array<IPerson>) => {
      this.dataSource = new MatTableDataSource(response);
      console.log('this.dataSource', this.dataSource);
      this.spinner.hide();
    }).catch((error: any) => {
      this.spinner.hide();
      console.log('error', error);
    });
  }
}
