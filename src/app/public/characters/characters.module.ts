import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharactersComponent } from './characters.component';
import { CharactersRoutingModule } from './characters-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from '../components/table/table.module';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [CharactersComponent],
  imports: [
    CommonModule,
    CharactersRoutingModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    NgxSpinnerModule
    
  ]
})
export class CharactersModule { }
