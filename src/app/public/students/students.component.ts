import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IPerson } from '../models/person.model';
import { HogwartsService } from '../services/hogwarts.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  public dataSource: MatTableDataSource<IPerson>;
  constructor(
    private hogwartsService: HogwartsService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,

  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.getStudents();

  }
  // Funtion get students
  public getStudents(): void {
    this.hogwartsService.getStudents().then((response: Array<IPerson>) => {
      this.dataSource = new MatTableDataSource(response);
      console.log('this.dataSource', this.dataSource);
      this.spinner.hide();
    }).catch((error: any) => {
      this.spinner.hide();
      console.log('error', error);
    });
  }

  open(content) {
    this.modalService.open(content);
  }
  public close() {
    this.modalService.dismissAll();
  }
  public studentData(e) {
    let data = this.dataSource.data;
    data.push(e);
    this.dataSource = new MatTableDataSource(data);
    this.close();
  }
}
