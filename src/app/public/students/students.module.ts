import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsComponent } from './students.component';
import { StudentsRoutingModule } from './students-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TableModule } from '../components/table/table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddStudentComponent } from '../components/add-student/add-student.component';
import {MatButtonModule} from '@angular/material/button';
@NgModule({
  declarations: [StudentsComponent, AddStudentComponent],
  imports: [
    CommonModule,
    StudentsRoutingModule,
    NgxSpinnerModule,
    TableModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule
  ]
})
export class StudentsModule { }
